# Origami-box

## Technical

```
pipenv shell
export PYTHONPATH=$(pwd)
```

## Informations

This package create a pdf file which could be used to print a file allowing nice origami box. The origami how-to is at the following youtube address ...

### Needs

- 1 square photo for the bottom of the box
- 4 rectangular photos for the 4 sides of the box
- 4 rectangular photos (will be croped with a triangle mask) for the closing flaps

# How to generate requirements.txt

```
pipenv run pip freeze > requirements.txt
or better (avoid dev packages)
pipenv lock -r > requirements.txt
```

# Geometry

## Rotation of a polygon

https://study.com/skill/learn/how-to-find-the-coordinates-of-a-polygon-after-a-rotation-explanation.html


# Image

## Paste

https://note.nkmk.me/en/python-pillow-paste/

## Rotate

- https://note.nkmk.me/en/python-pillow-rotate/
- https://www.stashofcode.fr/rotation-dun-point-autour-dun-centre/

```
/*------------------------------------------------------------------------------
Retourne l'image d'un point par une rotation (repère X de gauche à droite, Y du
haut vers le bas).

ENTREE :
	M		Point à transformer
	O		Point centre de la rotation
	angle	Angle (en degrés)

SORTIE :
	Image de M par la rotation d'angle angle autour de O (les coordonnées ne
	sont pas entières).
------------------------------------------------------------------------------*/

function rotate (M, O, angle) {
	var xM, yM, x, y;

	angle *= Math.PI / 180;
	xM = M.x - O.x;
	yM = M.y - O.y;
	x = xM * Math.cos (angle) + yM * Math.sin (angle) + O.x;
	y = - xM * Math.sin (angle) + yM * Math.cos (angle) + O.y;
	return ({x:Math.round (x), y:Math.round (y)});
}
```
